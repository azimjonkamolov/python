import mysql.connector

mydb=mysql.connector.connect(
    host = "localhost",
    user = "root",
    passwd = "",
    database = "projects"
)

mycursor = mydb.cursor()

go = "INSERT INTO info (user_name, user_pass) VALUES (%s, %s)"

people = [("Ali", 42),
          ("Boss", 23),
          ("Sos", 32),
          ("Ammar", 52),
          ("Kim", 22),]

mycursor.executemany(go, people)

mycursor = mydb.commit()