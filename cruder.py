# # App to connect into a database with ease
#  to practise
import mysql.connector

class CRUD:

    def __init__(self, hostname, username, passwd, dbname):
        self.hostname = hostname
        self.username = username
        self.passwd = passwd
        self.dbname = dbname

    def createdb(self):

        mydb = mysql.connector.connect(
            host = self.hostname,
            user = self.username,
            passwd = self.passwd
        )

        mycursor = mydb.cursor()

        print("Enter your SQL code here create db: ")
        action = input()

        mycursor.execute(action)

        mydb.commit()

    def createtb(self):

        mydb = mysql.connector.connect(
            host = self.hostname,
            user = self.username,
            passwd = self.passwd,
            db = self.dbname
        )

        mycursor = mydb.cursor()

        print("Enter your SQL code here create table: ")
        action = input()

        mycursor.execute(action)

        print("All the tables in " + dbname + ": ")
        mycursor.execute("SHOW TABLES")
        for tb in mycursor:
            print(tb)

    def writedb(self):

        mydb = mysql.connector.connect(
            host = self.hostname,
            user = self.username,
            passwd = self.passwd,
            db = self.dbname
        )

        mycursor = mydb.cursor()

        print("Enter your SQL code here to insert into db:")
        action = input()

        mycursor.execute(action)

        mydb.commit()

    def readdb(self):
        mydb = mysql.connector.connect(
            host=self.hostname,
            user=self.username,
            passwd=self.passwd,
            db =self.dbname
        )

        mycursor = mydb.cursor()

        print("Enter your SQL code here to read from db:")
        action = input()

        mycursor.execute(action)

        myresult = mycursor.fetchall()

        for row in myresult:
            print(row)

    def updatedb(self):
        mydb = mysql.connector.connect(
            host=self.hostname,
            user=self.username,
            passwd=self.passwd,
            db=self.dbname
        )

        mycursor = mydb.cursor()

        print("Enter your SQL code to update your table: ")

        action = input()

        mycursor.execute(action)

        mydb.commit()

    def delatedbORtb(self):
        mydb = mysql.connector.connect(
            host=self.hostname,
            user=self.username,
            passwd=self.passwd,
            db=self.dbname
        )

        mycursor = mydb.cursor()

        print("Enter your SQL code to delete db or table here:")
        action = input()

        mycursor.execute(action)

        mydb.commit()


print("\tWelcome TO SQL assistant")

hostname = "localhost"
username = "root"
passwd = ""
dbname = "dbname"

print("Your host's name is " + hostname)
print("Your username is " + username)
print("Your password is " + passwd)

print("Would you like to change them? Hit 1 if yes or just skip it")
choice = input()
if choice == '1':
    print()
    hostname = input()
    username = input()
    passwd = input()

    print("After the changes your info: ")
    print("Your host's name is " + hostname)
    print("Your username is " + username)
    print("Your password is " + passwd)

print("Would you like to create a database? if yes, hit 1 if not 0: ")
choice = input()
if choice == '1':
    check = CRUD(hostname=hostname, username=username, passwd=passwd)
    check.createdb()
else:
    print("Enter your db's name here please: ")
    dbname = input()
    check = CRUD(hostname=hostname, username=username, passwd=passwd, dbname=dbname)

    print("What actions do you want to do with your database?")
    print("0 >> CREATE A TABLE")
    print("1 >> INSERT")
    print("2 >> SELECT")
    print("3 >> UPDATE")
    print("4 >> DELETE")

    print("Enter your choice here: ")
    choice = input()
    if choice == '0':
        check.createtb()
    elif choice == '1':
        check.writedb()
    elif choice == '2':
        check.readdb()
    elif choice == '3':
        check.updatedb()
    elif choice == '4':
        check.delatedbORtb()
    else:
        print("Wrond input please try again:")


