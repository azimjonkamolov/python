import datetime # >> for the first one
from datetime import date # >> for the second one
import prac


print("Enter a word: ")
word = input()

if(prac.strLen(word)):
    print("The word consists of 4 or more characters")
else:
    print("The word is too short")


time = datetime.date.today() # >> the first one
# time = date.today() # >> the second one


print("The date is " + str(time))


# very easy