# To open a file
myFile = open("myFile.txt", "w")
myFile.write("Hello Python")
myFile.close()

# To get some info
print("File name: ", myFile.name)
print("File mode: ", myFile.mode)
print("File closed: ", myFile.closed)

# Not to overwrite use 'a' model append
myFile = open("myFile.txt", 'a')
myFile.write("\nI am here")
myFile.close()

# To read from file
myFile=open("myFile.txt", "r+")
text = myFile.read(10) # to show char limit insert a number into the brackets
print(text)
myFile.close()




#