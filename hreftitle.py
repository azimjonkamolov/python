import requests
from bs4 import BeautifulSoup
from html.parser import HTMLParser
from urllib.request import urlopen
import urllib


def error_callback(*_, **__):
    pass

def is_string(data):
    return isinstance(data, str)

def is_bytes(data):
    return isinstance(data, bytes)

def to_ascii(data):
    if is_string(data):
        data = data.encode('ascii', errors='ignore')
    elif is_bytes(data):
        data = data.decode('ascii', errors='ignore')
    else:
        data = str(data).encode('ascii', errors='ignore')
    return data


class Parser(HTMLParser):
    def __init__(self, url):
        self.title = None
        self.rec = False
        HTMLParser.__init__(self)
        try:
            self.feed(to_ascii(urlopen(url).read()))
        except urllib.error.HTTPError:
            return
        except urllib.error.URLError:
            return
        except ValueError:
            return

        self.rec = False
        self.error = error_callback

    def handle_starttag(self, tag, attrs):
        if tag == 'title':
            self.rec = True

    def handle_data(self, data):
        if self.rec:
            self.title = data

    def handle_endtag(self, tag):
        if tag == 'title':
            self.rec = False


def get_title(url):
    return Parser(url).title

siteName = 'http://www.bbc.com/'
fileStore = open("fileStore.txt", "w")

def trade_spider(max_pages):
    page = 1
    while page <= max_pages:
        url = siteName
        source_code = requests.get(url)
        plain_text = source_code.text
        soup = BeautifulSoup(plain_text)
        for link in soup.findAll('a'):
            href = link.get('href')
            if href[0] != '/' and href[0] != '#':
                toto = get_title(href)
                print("Title:" + toto)
                print("Url:" + href)
                fileStore.write(href + "\n")
        page += 1
    fileStore.close()

trade_spider(10)