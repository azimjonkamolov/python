# How to deal with lists in python
numbers = [1,2,3,4,5] # most popular way
print(numbers)

numbers = list((1,2,3,4,5)) # not usual way
print(type(numbers))

names = ["John", "Anna", "Mano", "Noma" , 1] # it does not matter what data type is used here

names.remove(1)

print(len(names))

print(names.sort())

# print(names.sort(reverse=True))

names.insert(2, "Tom")

names=names.pop(3)

print(names)

names = ["Lol", "Bol"]

names.append("Kim")

print(names)

names.pop(1)

print(names)











#