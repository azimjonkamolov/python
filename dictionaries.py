# Dictionaries

# To create a simple dict
person = {'first_name' : 'John', 'age' : 30}

# To create a dict using a constructor
# person1 = dict(last_name = 'Bob', age= 30)

# person(person['first_name']) without get couldnot get to work

print(person.get('first_name'))

# Add key/value
person['phone'] = '555-5555-5555'
print(person)

#Get keys
print(person.keys())

#Get items
print(person.items())


#To make copy
# person2 = person.copy()
#
# print(person2)


#To remove item
del (person['age'])
print(person)

#To remove item using pop
person.pop('phone')
print(person)

# To clear we can use .clear
# person.clear()

# Get length
print(len(person))


# List of dicts
people = [
    {'name' : 'Nana' , 'age' : 40},
    {'name' : "Anan" , 'age' : 30},
]

print(people[1]['age']) # should get 30



















#