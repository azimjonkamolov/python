name = "Azimjon"
print(name) # to print name out
print("Hello " + name)

age = 23    # to get age var with 37 int data type
print("My name is " + name + " my age is " + str(age))  # to turn int data type into a string data type

print("{},{},{}".format('a','b','c')) # Arguments by position
print("{2},{1},{0}".format('a','b','c')) # Arguments by position

print("{name} is {age} years old".format(name = name, age = age)) # Arguments by name

# print(f"{name} is {age} years old") >> only works with python 3.6 +

s= "hello there you"

print(s.capitalize()) # To make first letter capitalized

print(s.upper())    # To make all upper case

print(s.lower())    # To make all lower case

print(s.replace('you', 'me'))   # To replace word you to me

print(s.count('e')) # To count how many times 'e' used









#