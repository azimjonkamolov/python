import speech_recognition as sr

r = sr.Recognizer()
with sr.Microphone() as source:
    print("Speak Anything :")
    audio = r.listen(source)
    try:
        text = r.recognize_google(audio)
        print("You said : {}".format(text))
    except:
        print("Sorry could not recognize what you said")

# import speech_recognition
# import pyttsx
#
# speech_engine = pyttsx.init('sapi5') # see
# speech_engine.setProperty('rate', 150)
#
# def speak(text):
#    speech_engine.say(text)
#    speech_engine.runAndWait()
#
# recognizer = speech_recognition.Recognizer()
#
# def listen():
#     with speech_recognition.Microphone() as source:
#         recognizer.adjust_for_ambient_noise(source)
#         audio = recognizer.listen(source)
#
#     try:
#         return recognizer.recognize_sphinx(audio)
#         # or: return recognizer.recognize_google(audio)
#     except speech_recognition.UnknownValueError:
#         print("Could not understand audio")
#     except speech_recognition.RequestError as e:
#         print("Recog Error; {0}".format(e))
#
#     return ""
#
#
#
# speak("Say something!")
# speak("I heard you say " + listen())