# A function is a block of code which only runs when it is called

# A simple function with one argument
def sayHello(name):
    print("Hello " + name)
sayHello('Tom')

# A function with default argumant
def sayName(name="user"):
    print("Hello " + name)

sayName()           # in this case user will pop up
sayName('Jack')     # in this case input name will pop up

# A simple cal function
def getSum(num1, num2):
    total = num1+num2
    return total

print(getSum(1,3)) # the anser will be 4


# A lambde function

getTotal = lambda num1, num2 : num1 + num2
print(getTotal(4,6)) # the answer will be 10